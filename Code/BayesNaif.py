#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import time
import copy
import math

class BayesNaif:

    def __init__(self, **kwargs):
        # Ce dictionnaire contiendra des classes comme clés et un dictionaire
        # de variables pour chaque classe
        self.parametres = dict()

        # Traitement différent si Iris, car cas continu
        if kwargs.has_key('iris') :
	        self.Iris = kwargs['iris']
        else :
	        self.Iris = False

    def train(self, train, train_labels):
        start_time = time.clock()

        if not self.Iris :
            self.trainDiscret(train, train_labels)
        else :
            self.trainContinu(train, train_labels)
            
        print("Temps d'exécution entrainement : " + '{:0.2f}'.format(time.clock() - start_time) + " secondes.\n")

        # Test sur données entrainement
        print("Test avec données entrainement")
        self.test(train, train_labels)

    def trainDiscret(self, train, train_labels):
        # Calcul tableau des paramètres
        # Le tableau des paramètres va contenir l'occurence de chaque valeur
        # de chaque variable pour chaque classe selon les valeurs de train
        nbDonnees = len(train)
        for i in range(0, nbDonnees) :
            data = train[i]
            label = train_labels.item(i)

            # Si classe pas déjà vue, on ajoute
            if not label in self.parametres :
                self.parametres[label] = dict()    

            currentClass = self.parametres[label]

            for j in range(0, data.size) :
                d = data.item(j)
                
                # Si variable pas déjà vue, on ajoute
                if not j in currentClass :
                    currentClass[j] = dict()

                variable = currentClass[j]

                # Si valeur pas déjà vue pour variable, on initialise à 1
                if not d in variable :
                    variable[d] = 1
                # Sinon on incrémente
                else :
                    variable[d] += 1
        
        # Calcul du tableau de probabilités conditionnelles
        # On calcule ici toutes les P(v_i = x | c_j), les probabilités que
        # la variable i prenne la valeur x en connaissant que la classe est c_j
        self.probabilites = copy.deepcopy(self.parametres) # Pour copier la forme
        for i in self.parametres.keys():
            classe = self.parametres[i]

            # Calcul occurences totales de la classe
            nbOccurences = 0.0
            for valeur in classe[0].values() :
                nbOccurences += valeur

            for j in classe.keys() :
                variable = classe[j]
                for k in variable.keys() :
                    valeur = variable[k]
                    # probabilité = nbOccurencesValeur / nbOccurencesClasse
                    self.probabilites[i][j][k] = valeur / nbOccurences

            # On ajoute au vecteur de probabilité de la classe c_i sa probabilité propre P(c_i) avec la clé -1
            self.probabilites[i][-1] = nbOccurences/nbDonnees

    def trainContinu(self, train, train_labels):    
        # Calcul tableau des paramètres
        # Ici les paramètres sont un dictionnaire de variables. 
        # Pour chaque variable, on entrepose un vecteur de toutes les
        # valeurs rencontrées dans train
        nbDonnees = len(train)
        for i in range(0, nbDonnees) :
            data = train[i]
            label = train_labels.item(i)

            # Si classe pas déjà vue, on ajoute
            if not label in self.parametres :
                self.parametres[label] = dict()    

            currentClass = self.parametres[label]

            for j in range(0, data.size) :
                d = data.item(j)
                
                # Si variable pas déjà vue, on ajoute
                if not j in currentClass :
                    currentClass[j] = []

                # On ajoute la valeur courante à la variable
                variable = currentClass[j]
                variable.append(d)
        
        # Calcul du tableau de valeurs calculées
        # Comme on est dans le cas continu, on veut avoir un tableau qui contient
        # la moyenne et la variance de chacune des variables.
        # Les probabilités nécessaires seront ensuite déterminées dans predict()
        # sous l'hypothèse d'une distribution Gaussienne.
        self.valeursCalculees = dict()
        for i in self.parametres.keys():
            classe = self.parametres[i]

            self.valeursCalculees[i] = dict()

            # Calcul occurences totales de la classe
            nbOccurences = float(len(classe[0]))

            for j in classe.keys() :
                variable = classe[j]
                
                # Calcul moyenne
                moyenne = sum(variable)/nbOccurences

                # Calcul variance
                variance = 0
                for valeur in variable:
                    variance += (valeur - moyenne)**2
                variance /= nbOccurences

                # On aura donc, pour chaque variable, variable[0] = moyenne, variable[1] = variance
                self.valeursCalculees[i][j] = [moyenne, variance]

            # On ajoute au vecteur de la classe c_i sa probabilité propre P(c_i) avec la clé -1
            self.valeursCalculees[i][-1] = nbOccurences/nbDonnees
    
    def predict(self, exemple):
        if self.Iris :
            return self.predictContinu(exemple)
        else :
            return self.predictDiscret(exemple)
    
    def predictDiscret(self, exemple):
        # On va chercher directement les probabilités que chacune des variables
        # prenne la valeur présente dans exemple dans notre dictionnaire de
        # probabilités de chaque de classe. 
        #
        # NB : On ne calcule pas le facteur évidence, car il n'a pas d'impact sur notre
        # prédiction, comme il agit comme une constante par laquelle on divise toutes
        # les probabilités à posteriori.
        posteriori = dict()
        for i in self.probabilites.keys():
            classe = self.probabilites[i]

            # P(c_i | v_0 = x_0, ..., v_n = x_n) = P(c_i)*P(v_0 = x_0 | c_i)*...*P(v_n = x_n | c_i)
            prob = classe[-1]
            for j in classe.keys() :
                if j != -1 : 
                    variable = classe[j]
                    valeurFixee = exemple.item(j)
                    p_vj = variable[valeurFixee] if variable.has_key(valeurFixee) else 0  # probabilité que vj = exemple[j]
                    prob *= p_vj
            
            posteriori[i] = prob
        
        return max(posteriori, key=posteriori.get)

    def predictContinu(self, exemple):
        # On calcule ici la probabilité qu'une certaine variable prenne la valeur qu'elle a
        # dans exemple en supposant une distribution gaussienne des valeurs continues. On
        # calcule alors P(v_i = x_i | c_j) selon la moyenne et la variance de v_i, que 
        # l'on a entreposée dans notre dictionnaire valeursCalculees.
        #
        # NB : On ne calcule pas le facteur évidence, car il n'a pas d'impact sur notre
        # prédiction, comme il agit comme une constante par laquelle on divise toutes
        # les probabilités à posteriori.
        posteriori = dict()
        for i in self.valeursCalculees.keys():
            classe = self.valeursCalculees[i]

            prob = classe[-1]
            for j in classe.keys() :
                if j != -1 : 
                    variable = classe[j]
                    valeurFixee = exemple.item(j)
                    moyenne_j = variable[0]
                    variance_j = variable[1]
                    exposant = -((valeurFixee - moyenne_j)**2)/(2 * (variance_j ** 2))
                    denominateur = math.exp(exposant)
                    numerateur = math.sqrt(2 * math.pi) * variance_j
                    p_vj = denominateur/numerateur
                    prob *= p_vj
            
            posteriori[i] = prob
        
        return max(posteriori, key=posteriori.get)

    def test(self, test, test_labels):
        start_time = time.clock()
        compteurTotal = len(test)

        n = 3 if (self.Iris) else  2
        confusion = np.zeros(shape=(n,n))

        # On bâtit la matrice de confusion
        for i in range(0, compteurTotal):
            data = test[i]
            etiquette = test_labels.item(i)
            predict = self.predict(data)
            confusion[predict, etiquette] += 1

        if not self.Iris :
    		printConfusion(confusion, compteurTotal)
        else :
            # On doit séparer en 3 matrices de un-contre-tous pour le cas Iris
			print("Matrice de confusion incluant tous les cas\n")
			print(confusion.astype(int))
			print("\n")
			# On doit faire sortir 3 matrices de confusion dans le cas Iris
			for i in range(0, n):
				confusion_i = np.zeros(shape=(2,2))
				print("\nMatrice pour le cas " + str(i))
				# vraiPos
				confusion_i[0, 0] = confusion[i, i]
				# vraiNeg
				confusion_i[1, 1] = confusion[(i + 1) % n, (i + 1) % n] + confusion[(i + 1) % n, (i + 2) % n] + confusion[(i + 2) % n, (i + 1) % n] + confusion[(i + 2) % n, (i + 2) % n]
				# fauxPos
				confusion_i[0, 1] = confusion[i, (i + 1) % n] + confusion[i, (i + 2) % n]
				# faux Neg
				confusion_i[1, 0] = confusion[(i + 1) % n, i] + confusion[(i + 2) % n, i]

				printConfusion(confusion_i, compteurTotal)
        
        print("Temps d'exécution test : " + '{:0.2f}'.format(time.clock() - start_time) + " secondes.")

def printConfusion(confusion, compteurTotal) :
	# matrice de confusion
	print("Matrice de confusion\nLes colonnes sont les valeurs réelles, les lignes sont les valeurs prédites.\n")
	print(confusion.astype(int))
	print("\n")

	# (Vrais Positifs) / (Valeur réelle positive)
	recall = confusion[0][0]/float(confusion[0][0] + confusion[1][0]) * 100
	print("Recall : " + '{:0.2f}'.format(recall) + " %")

	# (Vrais Positifs) / (Valeur prédite positive)
	precision = confusion[0][0]/float(confusion[0][0] + confusion[0][1]) * 100
	print("Précision : " + '{:0.2f}'.format(precision) + " %")
	
	# (Vrais Positifs + Vrais Négatifs) / (Total)
	accuracy = (confusion[0][0] + confusion[1][1])/float(compteurTotal) * 100
	print("Accuracy : " + '{:0.2f}'.format(accuracy) + " %")