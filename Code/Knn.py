#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import heapq as heap
import math
import time

class Knn():

	def __init__(self, K = 5, **kwargs):
		self.CrossValidationTrain = False
		self.NbVoisins = K
		if 'iris' in kwargs.keys():
			self.Iris = kwargs['iris'] 
		else:
			self.Iris = False
		
		
	def train(self, train, train_labels): 
		start_time = time.clock()

		# Entrainement
		# Il suffit d'entreposer les voisins et leurs étiquettes respectives
		self.Voisins = train
		self.Voisins_Etiquettes = train_labels
		
		print("Temps d'exécution entrainement : " + '{:0.2f}'.format(time.clock() - start_time) + " secondes.\n")

		# Test sur données d'entrainement
		if not self.CrossValidationTrain:
			print("Test avec données entrainement")
			self.test(train, train_labels)


	def predict(self, exemple, K):
		# On visite tous les voisins, on extrait les K plus près de exemple
		# et la prédiction correspong à l'étiquette prédominante dans les
		# K voisins les plus près de l'exemple.

		voisinsEnOrdre = []

		# trouver distance avec tous les voisins
		for i in range(0, len(self.Voisins)):
			voisin = self.Voisins[i]
			etiquette = self.Voisins_Etiquettes.item(i)
			heap.heappush(voisinsEnOrdre, [distance(exemple, voisin), etiquette]) # Distance euclidienne

		voisinsPlusProches = []

		# trouver K voisins plus proches
		for i in range(0, K):
			voisinsPlusProches.append(heap.heappop(voisinsEnOrdre)[1])

		# prediction
		return max(set(voisinsPlusProches), key=voisinsPlusProches.count)


	def test(self, test, test_labels):
		start_time = time.clock()
		compteurTotal = len(test)

		n = 3 if (self.Iris) else 2
		confusion = np.zeros(shape=(n,n))

		# On bâtit la matrice de confusion
		for i in range(0, compteurTotal):
			data = test[i]
			etiquette = test_labels.item(i)
			predict = self.predict(data, self.NbVoisins)
			confusion[predict, etiquette] += 1

		if (not self.Iris) :
			printConfusion(confusion, compteurTotal)
		else :
			# Séparation en 3 matrices de un-contre-tous pour le cas Iris
			print("Matrice de confusion incluant tous les cas\n")
			print(confusion.astype(int))
			print("\n")
			# On doit faire sortir 3 matrices de confusion dans le cas Iris
			for i in range(0, n):
				confusion_i = np.zeros(shape=(2,2))
				print("\nMatrice pour le cas " + str(i))
				# vraiPos
				confusion_i[0, 0] = confusion[i, i]
				# vraiNeg
				confusion_i[1, 1] = confusion[(i + 1) % n, (i + 1) % n] + confusion[(i + 1) % n, (i + 2) % n] + confusion[(i + 2) % n, (i + 1) % n] + confusion[(i + 2) % n, (i + 2) % n]
				# fauxPos
				confusion_i[0, 1] = confusion[i, (i + 1) % n] + confusion[i, (i + 2) % n]
				# faux Neg
				confusion_i[1, 0] = confusion[(i + 1) % n, i] + confusion[(i + 2) % n, i]

				printConfusion(confusion_i, compteurTotal)

		print("Temps d'exécution test : " + '{:0.2f}'.format(time.clock() - start_time) + " secondes.")

	def crossValidate(self, data, labels):
		self.CrossValidationTrain = True
		partitionSize = int(math.floor(np.size(data,0)/10))
		labels = labels.getT()
		partitions = [data[0:partitionSize-1, 0:np.size(data,1)].copy(), data[partitionSize:2*partitionSize-1, 0:np.size(data,1)].copy(),
						  data[2*partitionSize:3*partitionSize-1, 0:np.size(data,1)].copy(), data[3*partitionSize:4*partitionSize-1, 0:np.size(data,1)].copy(),
						  data[4*partitionSize:5*partitionSize-1, 0:np.size(data,1)].copy(), data[5*partitionSize:6*partitionSize-1, 0:np.size(data,1)].copy(),
						  data[6*partitionSize:7*partitionSize-1, 0:np.size(data,1)].copy(), data[7*partitionSize:8*partitionSize-1, 0:np.size(data,1)].copy(),
						  data[8*partitionSize:9*partitionSize-1, 0:np.size(data,1)].copy(), data[9*partitionSize:np.size(data,0)-1, 0:np.size(data,1)].copy()]
		labelPartitions = [labels[0:partitionSize-1, 0:np.size(labels, 1)].copy(), labels[partitionSize:2*partitionSize-1, 0:np.size(labels,1)].copy(),
						   labels[2*partitionSize:3*partitionSize-1, 0:np.size(labels,1)].copy(), labels[3*partitionSize:4*partitionSize-1, 0:np.size(labels,1)].copy(),
						   labels[4*partitionSize:5*partitionSize-1, 0:np.size(labels,1)].copy(), labels[5*partitionSize:6*partitionSize-1, 0:np.size(labels,1)].copy(),
						   labels[6*partitionSize:7*partitionSize-1, 0:np.size(labels,1)].copy(), labels[7*partitionSize:8*partitionSize-1, 0:np.size(labels,1)].copy(),
						   labels[8*partitionSize:9*partitionSize-1, 0:np.size(labels,1)].copy(), labels[9*partitionSize:np.size(labels,0)-1, 0:np.size(labels,1)].copy()]
		errors = []
		for k in range(1, int(math.floor(np.size(data, 0)) / 3)):
			lMeanErrors = []
			for l in range(0,10):
				compressArray = []
				for i in range(1, np.size(data,0)):
					if i in range(l*partitionSize, (l+1)*partitionSize):
						compressArray.append(0)
					else:
						if l == 9 and i >= (l+1)*partitionSize:
							compressArray.append(0)
						else:
							compressArray.append(1)
				self.train(np.compress(compressArray, data, axis=0), np.compress(compressArray, labels, axis=0))
				lMeanError = 0
				for count, exemple in enumerate(partitions[l]):
					prediction = self.predict(exemple, k)
					if not self.Iris:
						if prediction != labelPartitions[l][count].item(0):
							lMeanError += 1 / np.size(partitions[l], 0)
					else:
						lMeanError += abs(prediction-labelPartitions[l][count][0]) / np.size(partitions[l], 1)
				lMeanErrors.append(lMeanError)
			errors.append(np.mean(lMeanErrors))
		self.NbVoisins = errors.index(min(errors)) + 1
		self.CrossValidationTrain = False
		return errors.index(min(errors)) + 1

def distance(a, b): # Euclidienne
	assert(0 < a.size)
	assert(a.size == b.size)

	compteur = 0
	for i in range(0, a.size):
		a_i = a.item(i)
		b_i = b.item(i)
		compteur += (a_i - b_i)**2
	
	return math.sqrt(compteur) # sqrt(sum((a_i - b_i)^2))

def printConfusion(confusion, compteurTotal) :
	# matrice de confusion
	print("Matrice de confusion\nLes colonnes sont les valeurs réelles, les lignes sont les valeurs prédites.\n")
	print(confusion.astype(int))
	print("\n")

	# (Vrais Positifs) / (Valeur réelle positive)
	recall = confusion[0][0]/float(confusion[0][0] + confusion[1][0]) * 100
	print("Recall : " + '{:0.2f}'.format(recall) + " %")

	# (Vrais Positifs) / (Valeur prédite positive)
	precision = confusion[0][0]/float(confusion[0][0] + confusion[0][1]) * 100
	print("Précision : " + '{:0.2f}'.format(precision) + " %")
	
	# (Vrais Positifs + Vrais Négatifs) / (Total)
	accuracy = (confusion[0][0] + confusion[1][1])/float(compteurTotal) * 100
	print("Accuracy : " + '{:0.2f}'.format(accuracy) + " %")
