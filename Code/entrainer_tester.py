import numpy as np
import sys
import load_datasets
import BayesNaif as bn # importer la classe du classifieur bayesien
import Knn as k # importer la classe du Knn

# Initializer vos paramètres
train_ratio = 0.6
print("Les datasets Iris et Congress seront traités avec un ratio d'entraînement de " + '{:0.2f}'.format(train_ratio * 100) + " %.\n")

# Initializer/instanciez vos classifieurs avec leurs paramètres
Bn_monk1 = bn.BayesNaif()
Bn_monk2 = bn.BayesNaif()
Bn_monk3 = bn.BayesNaif()
Bn_iris = bn.BayesNaif(iris = True)
Bn_congress = bn.BayesNaif()

Knn_monk1 = k.Knn()
Knn_monk2 = k.Knn()
Knn_monk3 = k.Knn()
Knn_iris = k.Knn(iris = True)
Knn_congress = k.Knn()

# Charger/lire les datasets
[train_monk1, train_labels_monk1, test_monk1, test_labels_monk1] = load_datasets.load_monks_dataset(1)
[train_monk2, train_labels_monk2, test_monk2, test_labels_monk2] = load_datasets.load_monks_dataset(2)
[train_monk3, train_labels_monk3, test_monk3, test_labels_monk3] = load_datasets.load_monks_dataset(3)
[train_iris, train_labels_iris, test_iris, test_labels_iris] = load_datasets.load_iris_dataset(train_ratio)
[train_congress, train_labels_congress, test_congress, test_labels_congress] = load_datasets.load_congressional_dataset(train_ratio)


# À commenter pour accélérer les résultats
# IFT-7025 (Evan)
# Effectuer la validation croisée pour trouver k optimal
data_monk1 = np.concatenate((train_monk1, test_monk1), axis=0)
labels_monk1 = np.concatenate((train_labels_monk1, test_labels_monk1), axis=1)
data_monk2 = np.concatenate((train_monk2, test_monk2), axis=0)
labels_monk2 = np.concatenate((train_labels_monk2, test_labels_monk2), axis=1)
data_monk3 = np.concatenate((train_monk3, test_monk3), axis=0)
labels_monk3 = np.concatenate((train_labels_monk3, test_labels_monk3), axis=1)
data_iris = np.concatenate((train_iris, test_iris), axis=0)
labels_iris = np.concatenate((train_labels_iris, test_labels_iris), axis=1)
data_congress = np.concatenate((train_congress, test_congress), axis=0)
labels_congress = np.concatenate((train_labels_congress, test_labels_congress), axis=1)

print("***** VALIDATION CROISÉE ******")
print("\ncross validate Knn Monk 1")
Knn_monk1.crossValidate(data_monk1, labels_monk1)
print("\ncross validate Knn Monk 2")
Knn_monk2.crossValidate(data_monk2, labels_monk2)
print("\ncross validate Knn Monk 3")
Knn_monk3.crossValidate(data_monk3, labels_monk3)
print("\ncross validate Knn Congress")
Knn_congress.crossValidate(data_congress, labels_congress)
print("\ncross validate Knn Iris")
Knn_iris.crossValidate(data_iris, labels_iris)


# Entrainez votre classifieur
print("***** ENTRAINEMENT *****")
print("\nTrain BayesNaif Monk 1")
Bn_monk1.train(train_monk1, train_labels_monk1)
print("\nTrain BayesNaif Monk 2")
Bn_monk2.train(train_monk2, train_labels_monk2)
print("\nTrain BayesNaif Monk 3")
Bn_monk3.train(train_monk3, train_labels_monk3)
print("\nTrain BayesNaif Iris")
Bn_iris.train(train_iris, train_labels_iris)
print("\nTrain BayesNaif Congress")
Bn_congress.train(train_congress, train_labels_congress)

print("\nTrain Knn Monk 1")
Knn_monk1.train(train_monk1, train_labels_monk1)
print("\nTrain Knn Monk 2")
Knn_monk2.train(train_monk2, train_labels_monk2)
print("\nTrain Knn Monk 3")
Knn_monk3.train(train_monk3, train_labels_monk3)
print("\nTrain Knn Iris")
Knn_iris.train(train_iris, train_labels_iris)
print("\nTrain Knn Congress")
Knn_congress.train(train_congress, train_labels_congress)


# Tester votre classifieur
print("\n\n***** TEST *****")
print("\nTest BayesNaif Monk 1")
Bn_monk1.test(test_monk1, test_labels_monk1)
print("\nTest BayesNaif Monk 2")
Bn_monk2.test(test_monk2, test_labels_monk2)
print("\nTest BayesNaif Monk 3")
Bn_monk3.test(test_monk3, test_labels_monk3)
print("\nTest BayesNaif Iris")
Bn_iris.test(test_iris, test_labels_iris)
print("\nTest BayesNaif Congress")
Bn_congress.test(test_congress, test_labels_congress)

print("\nTest Knn Monk 1")
Knn_monk1.test(test_monk1, test_labels_monk1)
print("\nTest Knn Monk 2")
Knn_monk2.test(test_monk2, test_labels_monk2)
print("\nTest Knn Monk 3")
Knn_monk3.test(test_monk3, test_labels_monk3)
print("\nTest Knn Iris")
Knn_iris.test(test_iris, test_labels_iris)
print("\nTest Knn Congress")
Knn_congress.test(test_congress, test_labels_congress)

