﻿**** Description des classes ****

loader_datasets.py
Il s'agit de la classe qui se charge de lire les données présentes dans les datasets et de les retourner sous la forme de matrices d'entraînement et de tests avec leurs étiquettes associées.

BayesNaif.py
Classificateur bayésien naïf. Appeler le constructeur avec "iris = True" pour gérer un cas continu (il est aussi pris pour acquis que le cas appelé de cette façon n'est pas binaire). Voir entrainer_tester pour exemples

Knn.py
Classificateur des K voisins les plus proches. Si aucun argument passé au constructeur, K = 5 par défaut. On peut passer un K précis au constructeur et aussi ajouter l'option "iris=True" pour traiter le cas Iris (simplement pour l'affichage des matrices de confusion comme les cas Iris n'est pas binaire). Voir entrainer_tester pour exemples

entrainer_tester.py
Main du programme. Va lire tous les datasets fournis, les entraîne et les teste avec les 2 méthodes énoncées plus haut. La validation croisée est aussi appelée.

**** Répartition des tâches ****
Evan : Load dataset Congress, BayesNaif (cas discret), Knn (validation croisée), documentation de la classe Knn dans le rapport et discussion des résultats

Mathieu : Load datasets Monks et Iris, Knn, BayesNaif (cas continu), output confusion matrix et autres valeurs, documentation de la classe BayesNaif dans le rapport et explication des termes

**** Difficultés rencontrées ****

La principale difficulté rencontrée est certainement survenue au moment de traduire les méthodes en code. En effet, bien que les méthodes soient bien documentées, en produire une version assez souple pour être utilisée avec les datasets très divers employés exigeait un certain devoir de planification.